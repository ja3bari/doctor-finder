package com.example.laithbzour.doctor;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ToxicBakery.viewpager.transforms.RotateUpTransformer;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;

import java.util.ArrayList;

public class Log_file  extends Fragment {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    public static MobileServiceClient mClient;
    public MobileServiceTable<favorate_dr> doctor_finder;
    public MobileServiceTable<doctor> doctor_finders;
    public static MobileServiceList<favorate_dr> result;
    public static MobileServiceList<doctor> results;



    private ViewPager mViewPager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_log_file, container, false);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());

        mViewPager = (ViewPager)rootView.findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout)rootView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        mViewPager.setPageTransformer(true, new RotateUpTransformer());
        try {

            mClient = new MobileServiceClient(
                    "https://docotrfinder.azure-mobile.net/",
                    "OCecmhorpCrBqDmLWZbaTporlbzoKP57",
                    getActivity()
            );

        } catch (Exception e) {
            createAndShowDialog(e, "Error");
        }
        return rootView;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }


        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        private Search_List_Adapter_2 adapter;
        ListView list;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_log_file, container, false);
            list = (ListView) rootView.findViewById(R.id.list_log_file);
            run_2();
            return rootView;
        }

        public void  run_2(){

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        final String Users_ID=Profile.user_id.toString();
                        result = mClient.getTable(favorate_dr.class).where().field("user_id").eq(Users_ID)
                                .execute().get();
                        if(result!=null)
                        {    for (final favorate_dr item : result) {

                            String Craft_Id=item.doctor_id.toString();
                            results = mClient.getTable(doctor.class).where().field("id").eq(Craft_Id)
                                    .execute().get();
                        }
                        }


                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }

                    return null;
                }
                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if(results!=null)
                            {
                                final ArrayList<String> Craft_id = new ArrayList<String>();
                                ArrayList<String> name = new ArrayList<String>();
                                ArrayList<String> job = new ArrayList<String>();
                                for (final doctor items : results)
                                {
                                    Craft_id.add(items.mId);
                                    name.add(items.name);
                                    job.add(items.speciality);
                                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                        @Override
                                        public void onItemClick(AdapterView<?> arg0, View arg1,
                                                                int position, long arg3) {
                                            Intent intent = new Intent(getContext(), Show_dr.class);
                                            intent.putExtra("id", Craft_id.get(position));
                                            startActivity(intent);

                                        }
                                    });

                                }
                                adapter= new Search_List_Adapter_2(getActivity(),name,job);
                                list.setAdapter(adapter);
                            }




                        }

                    });
                }
            }.execute();

        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {


        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return PlaceholderFragment.newInstance(position + 1);
                case 1:
                    Schedule_Acticity tab2 = new Schedule_Acticity();
                    return tab2;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Favorites list";
                case 1:
                    return "Schedule";

            }
            return null;
        }
    }




    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if (exception.getCause() != null) {
            ex = exception.getCause();
        }
        createAndShowDialog(ex.getMessage(), title);
    }

    private void createAndShowDialog(final String message, final String title) {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());

        builder.setMessage(message);
        builder.setTitle(title);
        builder.create().show();
    }
}
