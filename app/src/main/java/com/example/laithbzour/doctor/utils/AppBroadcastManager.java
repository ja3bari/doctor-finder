package com.example.laithbzour.doctor.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.example.laithbzour.doctor.BuildConfig;

public final class AppBroadcastManager {

    public static final String UPDATE_UI_ACTION = BuildConfig.APPLICATION_ID + ".update_ui_action";
    public static final String UPDATE_LANGUAGE_ACTION = BuildConfig.APPLICATION_ID + ".update_language_action";

    private Context mContext;
    private LocalBroadcastManager mBroadcastManager;
    private static AppBroadcastManager sBroadcastManager;

    private AppBroadcastManager(Context context) {
        mContext = context.getApplicationContext();
        mBroadcastManager = LocalBroadcastManager.getInstance(mContext);
    }

    public static synchronized AppBroadcastManager getInstance(Context context) {
        if (sBroadcastManager == null)
            sBroadcastManager = new AppBroadcastManager(context);
        return sBroadcastManager;
    }

    public void sendBroadcast(String action) {
        sendBroadcast(action, null);
    }

    public void sendBroadcast(String action, Bundle bundle) {
        try {
            Intent intent = new Intent(action);
            if (bundle != null)
                intent.putExtras(bundle);
            mBroadcastManager.sendBroadcast(intent);
        }
        catch (Exception ignored) {
        }
    }

    /**
     * Register for all predefined BroadcastReceiver actions like {@link AppBroadcastManager#UPDATE_LANGUAGE_ACTION UPDATE_LANGUAGE_ACTION}
     *
     * @param receiver {@link BroadcastReceiver}
     */
    public void registerReceiver(BroadcastReceiver receiver) {
        registerReceiver(receiver, UPDATE_UI_ACTION);
        registerReceiver(receiver, UPDATE_LANGUAGE_ACTION);
    }

    public void registerReceiver(BroadcastReceiver receiver, String action) {
        registerReceiver(receiver, new IntentFilter(action));
    }

    public void registerReceiver(BroadcastReceiver receiver, IntentFilter intentFilter) {

        try {
            mBroadcastManager.registerReceiver(receiver, intentFilter);
        }
        catch (Exception ignored) {
        }
    }

    public void unregisterReceiver(BroadcastReceiver receiver) {
        try {
            mBroadcastManager.unregisterReceiver(receiver);
        }
        catch (Exception ignored) {
        }
    }
}
