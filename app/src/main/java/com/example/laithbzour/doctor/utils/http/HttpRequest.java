package com.example.laithbzour.doctor.utils.http;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.SystemClock;
import android.widget.Toast;

import com.example.laithbzour.doctor.utils.AppInstance;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A helper class for handling HTTP requests
 */
public final class HttpRequest {

    public final String url;
    public final Headers headers;
    public final GenericRequestBody requestBody;
    private final AtomicBoolean cancel;

    private HttpRequest(String url, Headers headers, GenericRequestBody requestBody) {
        this.url = getProperUrl(url);
        this.headers = headers;
        this.requestBody = requestBody;
        this.cancel = new AtomicBoolean(false);
    }

    /**
     * Cancel the current {@link HttpRequest}
     */
    public void cancel() {
        cancel.set(true);
    }

    public ResponseData<String> execute() {
        return callHttpRequest();
    }

    private ResponseData<String> callHttpRequest() {

        int tries = 0;
        boolean success = false;
        ResponseData<String> result = null;

        while (tries < 3 && !cancel.get() && !success) {
            if (!isConnected(AppInstance.getInstance())) {
                result = new ResponseData<>();
                result.errorType = ErrorType.NETWORK;
                break;
            }

            SystemClock.sleep(tries * 2000); // Back-off time

            result = callHttpRequestUsingOkhttp();

            success = result.errorType == ErrorType.SUCCESS;
            tries++;
        }

        return result;
    }

    private ResponseData<String> callHttpRequestUsingOkhttp() {
        ResponseData<String> responseObj = new ResponseData<>();
        try {
            Request.Builder requestBuilder = new Request.Builder().url(url);
            if (headers != null) {
                requestBuilder.headers(headers);
            }
            if (requestBody != null) {
                requestBuilder.post(requestBody.prepareRequestBody(cancel));
            }

            Response response = new OkHttpClient().newCall(requestBuilder.build()).execute();

            String body = response.body() != null ? response.body().string() : null;
            responseObj.result = body;
            if (response.isSuccessful() && !cancel.get()) {
                responseObj.errorType = ErrorType.SUCCESS;
//                TimeUtil.saveServerTime(body); // TODO
            }
        }
        catch (IOException ignored) {
            responseObj.errorType = ErrorType.NETWORK;
        }
        catch (Exception ignored) {
            responseObj.errorType = ErrorType.GENERAL;
        }
        return responseObj;
    }


    private static final String getProperUrl(String url) {
        return url;
    }

    /**
     * Check if there is any connected network to the device and show a {@link Toast} message if there id no internet connection
     *
     * @param context
     * @param connectionType like {@link ConnectivityManager#TYPE_WIFI}, {@link ConnectivityManager#TYPE_MOBILE}
     * @return
     */
    public static boolean isConnected(Context context, Integer... connectionType) {

        boolean isConnected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork != null) {
            isConnected = activeNetwork.isConnectedOrConnecting();
            if (connectionType != null && connectionType.length != 0) {
                isConnected = isConnected && activeNetwork.getType() == connectionType[0];
            }
        }
        return isConnected;
    }

    public static final class Builder {

        private String url;
        private GenericRequestBody requestBody;
        private Headers headers;

        public Builder(String url) {
            this.url = url;
        }

        /**
         * Use this to post a JSON text
         *
         * @param jsonObject
         * @return
         */
        public Builder jsonBody(JSONObject jsonObject) {
            return jsonBody(jsonObject != null ? jsonObject.toString() : null);
        }

        /**
         * Use this to post a JSON text
         *
         * @param jsonText
         * @return
         */
        public Builder jsonBody(String jsonText) {
            return requestBody(new JsonRequestBody(jsonText));
        }

        /**
         * Use this to post different request body
         *
         * @param requestBody
         * @return
         * @see {@link JsonRequestBody}
         */
        public Builder requestBody(GenericRequestBody requestBody) {
            this.requestBody = requestBody;
            return this;
        }

        /**
         * Add headers here
         *
         * @param headers
         * @return
         */
        public Builder headers(Headers headers) {
            this.headers = headers;
            return this;
        }

        public HttpRequest build() {
            return new HttpRequest(url, headers, requestBody);
        }

        public ResponseData<String> execute() {
            return build().execute();
        }
    }
}
