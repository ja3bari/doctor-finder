
package com.example.laithbzour.doctor;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.laithbzour.doctor.gcm.GcmManager;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableOperationCallback;

import java.io.ByteArrayOutputStream;
import java.util.UUID;


public class SignUp_Doctor extends Fragment {

    public static MobileServiceClient mClient;
    public static MobileServiceTable<master_login> Doctor_Finder_2;
    public static MobileServiceTable<doctor> Doctor_Finder;
    private ImageView profile_image;
    public static final int RESULT_OK = -1;
    final int TAKE_PICTURE = 1;
    final int ACTIVITY_SELECT_IMAGE = 2;
    private EditText username, email, password, conf_pass, mobile,price,description;
    private Spinner Insurance, Specialization;
    private Button signup_btn_1;
    public static final String LOCATION_SERVICE = "location";
    private CheckBox show;
    String SENDER_ID ;
    private GoogleCloudMessaging gcm = null;
    private MobileServiceList<master_login> result;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sign_up_doctor, container, false);
        getActivity().getWindow().getAttributes().windowAnimations = R.anim.push_left;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        try {

            mClient = new MobileServiceClient(
                    "https://docotrfinder.azure-mobile.net/",
                    "OCecmhorpCrBqDmLWZbaTporlbzoKP57",
                    getActivity()
            );

        } catch (Exception e) {
            createAndShowDialog(e, "Error");
        }
        username = (EditText) rootView.findViewById(R.id.username);
        email = (EditText) rootView.findViewById(R.id.email);
        password = (EditText) rootView.findViewById(R.id.password);
        conf_pass = (EditText) rootView.findViewById(R.id.conf_password);
        mobile = (EditText) rootView.findViewById(R.id.mobile);
        price = (EditText) rootView.findViewById(R.id.price);
        Insurance = (Spinner) rootView.findViewById(R.id.Inc);
        Specialization = (Spinner) rootView.findViewById(R.id.spec);
        profile_image = (ImageView) rootView.findViewById(R.id.btnSelectPhoto_2);
        show = (CheckBox) rootView.findViewById(R.id.show_password);
        signup_btn_1 = (Button) rootView.findViewById(R.id.signup_btn);

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (show.isChecked()) {
                    password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                }
            }
        });

        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        signup_btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                singUp();
            }
        });

            SENDER_ID= GcmManager.getRegId(getContext());


        return rootView;
    }


    private void selectImage() {

        final CharSequence[] options = {"Take Picture From Camera", "Pick Picture From Gallery", "Close"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Picture !!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                if (options[which].equals("Take Picture From Camera")) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, TAKE_PICTURE);
                } else if (options[which].equals("Pick Picture From Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, ACTIVITY_SELECT_IMAGE);
                } else if (options[which].equals("Close")) {
                    dialog.dismiss();
                }

            }
        });
        builder.show();

    }

    String img;

    @Override
    public void onActivityResult(int requestcode, int resultcode, Intent intent) {
        super.onActivityResult(requestcode, resultcode, intent);
        Context context = getContext();
        if (resultcode == RESULT_OK) {
            if (requestcode == TAKE_PICTURE) {
                Bitmap photo = (Bitmap) intent.getExtras().get("data");
                Drawable drawable = new BitmapDrawable(photo);
                img = bitmapToBase64(photo);
                profile_image.setImageDrawable(drawable);


            } else if (requestcode == ACTIVITY_SELECT_IMAGE) {
                Uri selectedImage = intent.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = context.getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                Drawable drawable = new BitmapDrawable(thumbnail);
                profile_image.setImageDrawable(drawable);
                img = bitmapToBase64(thumbnail);
                profile_image.setRotation(270);

            }
        }
    }

    public String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        return imgString;
    }

    UUID uuid = UUID.randomUUID();
    String uuidInString = uuid.toString();
    String newid;

    public void singUp() {
        if (!validate()){
                onSignupFailed();
                return;
        }

            startTask();


    }

    private SignUpTask mSignUpTask;

    private void startTask() {
        cancelTask();

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            GPSTracker gps;
            gps = new GPSTracker(getActivity());
            final String userName = username.getText().toString();
            final String userEmail = email.getText().toString();
            final String userPassword = password.getText().toString();
            final String userMobile = mobile.getText().toString();
            final String userImage = img;
            final String latidude = String.valueOf(gps.getLatitude());
            final String userLong = String.valueOf(gps.getLongitude());
            final String Incurance = Insurance.getSelectedItem().toString();
            final String spez = Specialization.getSelectedItem().toString();
            final String dr_price = price.getText().toString();
            final String user_dv_id = SENDER_ID;

            mSignUpTask = new SignUpTask(userName, userEmail, userPassword,
                    userMobile, userImage, latidude, userLong, Incurance, spez,user_dv_id,dr_price);
            mSignUpTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            showGPSDisabledAlertToUser();
        }

    }

    private void showGPSDisabledAlertToUser() {
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Close",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();

                            }
                        });
        alertDialogBuilder.setNegativeButton("GPS Setting",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent callGPSSettingIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(callGPSSettingIntent);
                    }
                });
        android.app.AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void cancelTask() {

        if (mSignUpTask != null)
            mSignUpTask.cancel(true);
    }

    private class SignUpTask extends AsyncTask {

        ProgressDialog progressDialog;
        final String UserName, UserEmail, UserPassword, UserPhone,
                UserImage, latidude, UserLongitude, Incurance, spez,user_dv_id,dr_price;
        boolean success;

        public SignUpTask(String UserName, String UserEmail, String UserPassword, String UserPhone, String UserImage
                , String latidude, String userLong, String Incurance, String spez,String user_dv_id,String dr_price) {
            this.UserName = UserName;
            this.UserEmail = UserEmail;
            this.UserPassword = UserPassword;
            this.UserPhone = UserPhone;
            this.UserImage = UserImage;
            this.latidude = latidude;
            this.UserLongitude = userLong;
            this.Incurance = Incurance;
            this.spez = spez;
            this.user_dv_id = user_dv_id;
            this.dr_price = dr_price;


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.show();
            progressDialog.setContentView(R.layout.custom_progressdialog);
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    cancelTask();
                }
            });
            try {
                progressDialog.show();
            } catch (Exception ignored) {
            }
        }

        @Override
        protected Object doInBackground(Object[] objects) {

            SystemClock.sleep(1000);

                if (isCancelled())
                    return null;
                final doctor item = new doctor();
                item.mId = uuidInString.toString();
                item.name = UserName;
                item.email = UserEmail;
                item.mobile_number = UserPhone;
                item.password = UserPassword;
                item.latidude = latidude;
                item.langitude = UserLongitude;
                item.type = String.valueOf("doctor");
                item.insurance_company = Incurance.toString();
                item.speciality = spez.toString();
                item.image = img;
                item.favo = String.valueOf(0);
                item.rating = String.valueOf(0);
                item.price = dr_price;
                item.deviceid = SENDER_ID.toString();

                mClient.getTable(doctor.class).insert(item, new TableOperationCallback<doctor>() {
                    public void onCompleted(doctor entity, Exception exception, ServiceFilterResponse response) {
                        if (exception == null) {
                            newid = item.mId.toString();
                            onSignupSuccess();
                            username.setText("");
                            email.setText("");
                            password.setText("");
                            conf_pass.setText("");
                            mobile.setText("");
                            price.setText("");
                            Intent intent = new Intent(getActivity(), DoctorProfile.class);
                            intent.putExtra("id", item.mId.toString());
                            startActivity(intent);


                        } else {
                            Toast.makeText(getContext(), "Error !!", Toast.LENGTH_LONG).show();
                        }
                    }
                });


                success = true;


                return null;

            }


        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            try {
                SystemClock.sleep(10000);
            } catch (Exception e) {
            }

            if (!success) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "Error !!", Toast.LENGTH_LONG).show();
            }

        }
    }

    public void onSignupSuccess() {
        final master_login items = new master_login();
        items.mId = newid.toString();
        items.m_email = email.getText().toString();
        items.m_password = password.getText().toString();
        items.m_type = String.valueOf("doctor");

        mClient.getTable(master_login.class).insert(items, new TableOperationCallback<master_login>() {
            @Override
            public void onCompleted(master_login entity, Exception exception, ServiceFilterResponse response) {
                if (exception == null) {
                    Toast.makeText(getContext(), "Register Success", Toast.LENGTH_LONG).show();
                    cancelTask();
                } else {

                    onSignupFailed();

                }
            }
        });


    }

    public void onSignupFailed() {
        cancelTask();
        Toast.makeText(getContext(), "Error !!", Toast.LENGTH_LONG).show();
    }
    public boolean validate() {
        boolean valid = true;
        String name = username.getText().toString();
        String email_user = email.getText().toString();
        String user_password = password.getText().toString();
        String mobile_user = mobile.getText().toString();
        String con_password = conf_pass.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            username.setError("Enter Valid Name");
            username.setText("");
            valid = false;
        } else {
            username.setError(null);
        }

        if (email_user.isEmpty() ||
                !android.util.Patterns.EMAIL_ADDRESS.matcher(email_user)
                        .matches()) {
            email.setError("Enter Valid Email");
            email.setText("");
            valid = false;
        } else {
            email.setError(null);
        }

        if (user_password.isEmpty() || password.length() < 8) {
            password.setError("Enter Password Between 8 and 10 Characters");
            password.setText("");
            valid = false;
        } else {
            password.setError(null);
        }
        if (con_password.isEmpty() || !con_password.equals(user_password)) {
            conf_pass.setError("Password not match");
            conf_pass.setText("");
            valid = false;
        } else {
            conf_pass.setError(null);
        }
        if (mobile_user.isEmpty() || mobile_user.length() < 10) {
            mobile.setError("Enter Valid Mobile Number");
            mobile.setText("");
            valid = false;
        } else {
            mobile.setError(null);
        }

        return valid;
    }

    @Override
    public void onDestroy() {
        cancelTask();
        super.onDestroy();
    }


    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if (exception.getCause() != null) {
            ex = exception.getCause();
        }
        createAndShowDialog(ex.getMessage(), title);
    }

    private void createAndShowDialog(final String message, final String title) {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());

        builder.setMessage(message);
        builder.setTitle(title);
        builder.create().show();
    }


    public void check() {
        startTask_2();
    }

    private LoginTask mLoginTask;

    private void startTask_2() {
        final String email_master = email.getText().toString();
        mLoginTask = new LoginTask(email_master);
        mLoginTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }



    private class LoginTask extends AsyncTask {

        ProgressDialog progressDialog;
        private MobileServiceList<master_login> result;
        final String email;
        boolean success;

        public LoginTask(String email) {
            this.email = email;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Object doInBackground(Object[] objects) {

            SystemClock.sleep(3000);
            if (isCancelled())
                return null;
            try {
                result = mClient.getTable(master_login.class).where().field("email").eq(email).execute().get();
                success = true;
            } catch (Exception exception) {
                return exception;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
            }

            if (!success) {
                startTask();
            }
            if (result == null) {
                startTask();
                return;
            }

            for (master_login item : result) {
                Toast.makeText(getContext(), "Match Email !!", Toast.LENGTH_LONG).show();
                return;
                }
            }

        }




}



