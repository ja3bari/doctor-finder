package com.example.laithbzour.doctor.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class AppPreferencesManager {

    private static AppPreferencesManager sInstance;
    private SharedPreferences mSharedPreferences;
    private Editor mEditor;

    private static final String DELIM = "@*";

    /**
     * Class constructor with context only. this will use the default shared preference, if you need to specify the shared preference name use the other constructor
     *
     * @param context
     */
    private AppPreferencesManager(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mSharedPreferences.edit();
        mEditor.apply();
    }

    /**
     * Class constructor with context and shared preference name with MODE_PRIVATE set as default, if you would like to use the default one use AppPreferencesManager(Context
     * context)
     *
     * @param context
     * @param preferenceName
     */
    public AppPreferencesManager(Context context, String preferenceName) {
        mSharedPreferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
        mEditor.apply();
    }

    public static synchronized AppPreferencesManager getInstance(Context context) {
        if (sInstance == null)
            sInstance = new AppPreferencesManager(context.getApplicationContext());

        return sInstance;
    }


    /**
     * the default value if the key does not exist will be false;
     *
     * @param key : target key of Preference Manager see: SamConstantInterface
     * @return
     */
    public boolean getBooleanValue(String key) {
        return getBooleanValue(key, false);
    }

    /**
     * @param key          : target key of Preference Manager see: SamConstantInterface
     * @param defaultValue : the default value of the Preference Manager to return if the key does not exist
     * @return
     */
    public boolean getBooleanValue(String key, boolean defaultValue) {
        return this.mSharedPreferences.getBoolean(key, defaultValue);
    }

    /**
     * Set boolean for given key
     *
     * @param key   : target key of Preference Manager to set the value for see: SamConstantInterface
     * @param value
     */
    public void setBooleanValue(String key, boolean value) {
        this.mEditor.putBoolean(key, value).apply();
    }

    /**
     * get String value from the preferences of the provided key
     *
     * @param key          : key to get it's value
     * @param defaultValue : default value to return in case data for key not found
     * @return
     */
    public String getStringValue(String key, String defaultValue) {
        return this.mSharedPreferences.getString(key, defaultValue);
    }

    /**
     * Add new String to the preferences
     *
     * @param key   : key of the new value to add
     * @param value : value for the key
     */
    public void setStringValue(String key, String value) {
        this.mEditor.putString(key, value).apply();
    }


    /**
     * get Long value from the preferences of the provided key
     *
     * @param key          : key to get it's value
     * @param defaultValue : default value to return in case data for key not found
     * @return
     */
    public long getLongValue(String key, long defaultValue) {
        return this.mSharedPreferences.getLong(key, defaultValue);
    }

    /**
     * set long value to the preferences with the provided key
     *
     * @param key
     * @param value
     */
    public void setLongValue(String key, long value) {
        this.mEditor.putLong(key, value).apply();
    }


    /**
     * get Int value from the preferences of the provided key
     *
     * @param key          : key to get it's value
     * @param defaultValue : default value to return in case data for key not found
     * @return
     */
    public int getIntValue(String key, int defaultValue) {
        return this.mSharedPreferences.getInt(key, defaultValue);
    }

    /**
     * Add new Int to the preferences with the provided key
     *
     * @param key   : key of the new value to add
     * @param value : value for the key
     */
    public void setIntValue(String key, int value) {
        this.mEditor.putInt(key, value).apply();
    }


    /**
     * get String value from the preferences of the provided key
     *
     * @param key : key to get it's value
     * @return
     */
    public List<String> getStringArrayValue(String key) {

        String value = getStringValue(key, null);
        return TextUtils.isEmpty(value) ? null : Arrays.asList(value.split(Pattern.quote(DELIM)));
    }

    /**
     * Add new String Array to the preferences
     *
     * @param key   : key of the new value to add
     * @param value : value for the key
     */
    public void setStringArrayValue(String key, List<String> value) {

        if (value == null || value.isEmpty()) {
            setStringValue(key, null);
            return;
        }

        boolean addDelim = false;
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : value) {
            if (addDelim)
                stringBuilder.append(DELIM);
            else
                addDelim = true;
            stringBuilder.append(s);
        }
        setStringValue(key, stringBuilder.toString());
    }


    /**
     * remove given key from the shared preferences file.
     *
     * @param key
     */
    public void removeValue(String key) {
        this.mEditor.remove(key).commit();
    }

    /**
     * Check for a key if exist in the shared preference which set on the constructor @see AppPreferencesManager(Context context) and AppPreferencesManager(Context context,
     * String
     * preferenceName
     *
     * @param key
     * @return
     */
    public boolean isKeyExist(String key) {
        return mSharedPreferences.contains(key);
    }

    /**
     * preform apply on the preferences manager mEditor for all values been set without committing, this methodology will make the application lighter, all you need is to
     * apply one
     * time after the values are set, unless you're adding one key
     */
    public void apply() {
        this.mEditor.apply();
    }

    /**
     * get shared preferences sInstance
     *
     * @return sInstance of mSharedPreferences object
     */
    public SharedPreferences getSharedPreferences() {
        return this.mSharedPreferences;
    }
}