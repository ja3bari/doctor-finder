package com.example.laithbzour.doctor;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;

public class Profile extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String PREFS_NAME = "preferences";
    public static MobileServiceClient mClient;
    public MobileServiceTable<user> salehli;
    public static String user_id,user_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        try {

            mClient = new MobileServiceClient(
                    "https://docotrfinder.azure-mobile.net/",
                    "OCecmhorpCrBqDmLWZbaTporlbzoKP57",
                    this
            );


        } catch (Exception e) {
            createAndShowDialog(e, "Error");
        }

        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.flFragments, new Search_for_doctor());
        tx.commit();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        run();
    }
    String name_1,email_1,image_profile;

    public void  run(){
        salehli  = mClient.getTable(user.class);
        new AsyncTask<Void, Void, Void>() {
            Intent intent = getIntent();
            final String Data = intent.getExtras().getString("id");

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final MobileServiceList<user> result =
                            salehli.where().field("id").eq(Data).execute().get();
                    for (final user item : result) {
                        user_id=Data.toString();
                        user_name=item.name.toString();
                        Log.v("Name Of User:", item.name);
                        name_1=item.name.toString();
                        email_1=item.email.toString();
                        image_profile=item.image.toString();
                    }

                } catch (Exception exception) {
                    exception.printStackTrace();
                }

                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                        View headView = navigationView.getHeaderView(0);
                        ((TextView) headView.findViewById(R.id.name_of_user)).setText(name_1.toString());
                        if (image_profile==null) {
                            ((ImageView) headView.findViewById(R.id.profile_image)).setImageResource(R.drawable.user_male);
                        } else{
                            Bitmap img=base64ToBitmap(image_profile);
                            Drawable drawable = new BitmapDrawable(img);
                            ((ImageView) headView.findViewById(R.id.profile_image)).setImageDrawable(drawable);
                            headView.findViewById(R.id.profile_image).setRotation(270);}
                    }
                });
            }
        }.execute();

    }
    private Bitmap base64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else
        {
            moveTaskToBack(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        Fragment fragment = new Fragment();
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            fragment = new Log_file();
        }
        else if (id == R.id.nav_gallery)
        {
            LocationManager locationManager = (LocationManager)this.getSystemService(LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            {
                fragment = new Search_for_doctor();
            }
            else
            {
                showGPSDisabledAlertToUser();
            }
        } else if (id == R.id.nav_slideshow)
        {
            fragment = new Update_Profile_User();

        }
        else if (id == R.id.nav_send) {
            new AlertDialog.Builder(Profile.this)
                    .setTitle("Singout")
                    .setMessage("Are You Sure For Singout ?")
                    .setPositiveButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    })
                    .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.clear();
                            editor.commit();
                            Intent intent_1 = new Intent(Profile.this, LoginActivity.class);
                            startActivity(intent_1);
                            finish();

                        }
                    })
                    .show();
        }

        transaction.replace(R.id.flFragments, fragment);
        transaction.commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showGPSDisabledAlertToUser(){
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Close",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();

                            }
                        });
        alertDialogBuilder.setNegativeButton("GPS Setting",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent callGPSSettingIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(callGPSSettingIntent);
                    }
                });
        android.app.AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if (exception.getCause() != null) {
            ex = exception.getCause();
        }
        createAndShowDialog(ex.getMessage(), title);
    }

    private void createAndShowDialog(final String message, final String title) {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);

        builder.setMessage(message);
        builder.setTitle(title);
        builder.create().show();
    }
}
