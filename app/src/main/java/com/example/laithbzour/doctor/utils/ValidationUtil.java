package com.example.laithbzour.doctor.utils;


import com.example.laithbzour.doctor.cnst.Flags;
import com.example.laithbzour.doctor.model.ConfigSplash;

public class ValidationUtil {

    public static int hasPath(ConfigSplash cs) {
        if (cs.getPathSplash().isEmpty())
            return Flags.WITH_LOGO;
        else
            return Flags.WITH_PATH;
    }
}
