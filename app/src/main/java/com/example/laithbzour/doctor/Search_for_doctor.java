package com.example.laithbzour.doctor;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import com.ToxicBakery.viewpager.transforms.RotateUpTransformer;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class Search_for_doctor extends Fragment {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    public static MobileServiceClient mClient;
    public static MobileServiceTable<doctor> Doctor_Finder;
    public static MobileServiceList<doctor> result;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_search_for_doctor2, container, false);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());

        mViewPager = (ViewPager)rootView.findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setPageTransformer(true, new RotateUpTransformer());

        //key of windows azure
        try {

            mClient = new MobileServiceClient(
                    "https://docotrfinder.azure-mobile.net/",
                    "OCecmhorpCrBqDmLWZbaTporlbzoKP57",
                    getActivity()
            );

        } catch (Exception e) {
            createAndShowDialog(e, "Error");
        }

        /////

        return rootView;

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }
        private Search_List_Adapter_2 adapter;
        public String userLat,userLong,dr_inc,dr_spec;
        public static final String LOCATION_SERVICE = "location";
        Button btnCustomDialog;
        private Spinner inc_spinner,spec_spinner;
        ListView list;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_search_for_doctor, container, false);
            //pick location of user

            GPSTracker gps;
            gps = new GPSTracker(getActivity());
            try {

                List<Address> addresses = null;
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                addresses = geocoder.getFromLocation(gps.latitude, gps.longitude, 1);
                userLat=String.valueOf(gps.getLatitude());
                userLong=String.valueOf(gps.getLongitude());

            }
            catch (IOException e) {
                e.printStackTrace();
            }

            btnCustomDialog = (Button)rootView.findViewById(R.id.Inc_btn);
            inc_spinner=(Spinner)rootView.findViewById(R.id.Insurance_spinner);
            spec_spinner=(Spinner)rootView.findViewById(R.id.Specializatio_spinner);
            list = (ListView) rootView.findViewById(R.id.list_search_2);
            btnCustomDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Search_craft();

                }
            });

            return rootView;
        }
        ProgressDialog progressDialog;
        boolean success;
        // class of search


        public void  Search_craft()
        {
            LocationManager locationManager = (LocationManager)getActivity().getSystemService(LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                progressDialog = new ProgressDialog(getContext());
                progressDialog.show();
                progressDialog.setContentView(R.layout.custom_progressdialog);

                Doctor_Finder = mClient.getTable(doctor.class);
                new AsyncTask<Void, Void, Void>() {

                    // take the latitude and longitude of user

                    final double user_latitude = Double.valueOf(userLat);
                    final double user_longitude = Double.valueOf(userLong);
                    final String incu = dr_inc = inc_spinner.getSelectedItem().toString();
                    final String spes = dr_spec = spec_spinner.getSelectedItem().toString();

                    @Override
                    protected Void doInBackground(Void... params) {

                        try {

                            result = mClient.getTable(doctor.class).where().field("insurance_company").eq(incu)
                                    .or().field("speciality").eq(spes).execute().get();
                            Collections.sort((List<doctor>) list, new MyOrderingClass(user_latitude, user_longitude) {
                            });
                            success = true;

                        }
                        catch (Exception exception) {
                            exception.printStackTrace();
                            progressDialog.dismiss();

                        }

                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        progressDialog.dismiss();
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<String> name = new ArrayList<String>();
                                ArrayList<String> incurance = new ArrayList<String>();
                                final ArrayList<String> dr_id = new ArrayList<String>();

                                for (final doctor item : result) {

                                    dr_id.add(item.mId);
                                    name.add(item.name);
                                    incurance.add(item.insurance_company);
                                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                        @Override
                                        public void onItemClick(AdapterView<?> arg0, View arg1,
                                                                int position, long arg3) {
                                            Intent intent = new Intent(getContext(), Show_dr.class);
                                            intent.putExtra("id", dr_id.get(position));
                                            startActivity(intent);

                                        }
                                    });

                                }
                                 // set item of list in adapter
                                adapter = new Search_List_Adapter_2(getActivity(), name, incurance);
                                list.setAdapter(adapter);

                            }

                        });


                    }
                }.execute();
            }
            else
            {
                showGPSDisabledAlertToUser();
            }

        }

        // compare nearest location of user
        public class MyOrderingClass implements Comparator<doctor> {

            double userLat, userLong;

            public MyOrderingClass(double userLat, double userLong) {
                this.userLat = userLat;
                this.userLong = userLong;
            }

            @Override
            public int compare(doctor x, doctor y) {
                double dist1 = GetDistanceTo(userLat, userLong, Double.valueOf(x.latidude), Double.valueOf(x.langitude));
                double dist2 = GetDistanceTo(userLat, userLong, Double.valueOf(y.latidude), Double.valueOf(y.langitude));

                if (dist1 < dist2)
                    return -1;
                else if (dist1 > dist2)
                    return 1;
                else
                    return 0;
            }


            //calculate the Distance between 2 point
            public double GetDistanceTo(double fromLatitude, double fromLongitude, double toLatitude, double toLongitude)
            {
                if (Double.isNaN(fromLatitude) || Double.isNaN(fromLongitude)) {
                    throw new IllegalArgumentException("On of the given Latitude or Longitudes is not correct");
                }
                else {
                    double latitude = fromLatitude * 0.0174532925199433;
                    double longitude = fromLongitude * 0.0174532925199433;
                    double num = toLatitude * 0.0174532925199433;
                    double longitude1 = toLongitude * 0.0174532925199433;
                    double num1 = longitude1 - longitude;
                    double num2 = num - latitude;
                    double num3 =
                            Math.pow(Math.sin(num2 / 2), 2) + Math.cos(latitude) * Math.cos(num) * Math.pow(Math.sin(num1 / 2), 2);
                    double num4 = 2 * Math.atan2(Math.sqrt(num3), Math.sqrt(1 - num3));
                    double num5 = 6376500 * num4;

                    return num5;
                }
            }
        }


        private void showGPSDisabledAlertToUser(){
            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
            alertDialogBuilder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                    .setCancelable(false)
                    .setPositiveButton("Close",
                            new DialogInterface.OnClickListener(){
                                public void onClick(DialogInterface dialog, int id){

                                    dialog.cancel();

                                }
                            });
            alertDialogBuilder.setNegativeButton("GPS Setting",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent callGPSSettingIntent = new Intent(
                                    android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(callGPSSettingIntent);
                        }
                    });
            android.app.AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return PlaceholderFragment.newInstance(position + 1);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "";
            }
            return null;
        }
    }


    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if (exception.getCause() != null) {
            ex = exception.getCause();
        }
        createAndShowDialog(ex.getMessage(), title);
    }

    private void createAndShowDialog(final String message, final String title) {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());

        builder.setMessage(message);
        builder.setTitle(title);
        builder.create().show();
    }
}
