package com.example.laithbzour.doctor.gcm;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.laithbzour.doctor.utils.AppPreferencesManager;
import com.example.laithbzour.doctor.utils.http.ErrorType;
import com.example.laithbzour.doctor.utils.http.HttpRequest;
import com.example.laithbzour.doctor.utils.http.HttpTask;
import com.example.laithbzour.doctor.utils.http.JsonRequestBody;
import com.example.laithbzour.doctor.utils.http.ResponseData;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.squareup.okhttp.Headers;

import org.json.JSONException;
import org.json.JSONObject;

public final class GcmManager {

    public static final String MESSAGE_KEY = "message";

    public static String getRegId(Context context) {
        return AppPreferencesManager.getInstance(context).getStringValue(RegistrationIntentService.TOKEN, null);
    }


    public static boolean startGCM(Activity activity) {

        if (checkPlayServices(activity)) {
            // Start IntentService to register this application with GCM.
            RegistrationIntentService.startService(activity);
            return true;
        }
        return false;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    public static boolean checkPlayServices(Activity activity) {
        final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(activity, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("TOKEN", "This device is not supported.");
                activity.finish();
            }
            return false;
        }
        return true;
    }

    private static final String API_KEY = "AIzaSyBINQQpqmF9ZDjqwkAqNeKS2dNEpfCONVk";

    public static void sendPushMessage(final Context context, String RegId, Integer dataVO,String date,String name) {
        String token = RegId;
        if (token != null) {

            JSONObject object = new JSONObject();
            try {
                object.put("to", token);

                JSONObject data = new JSONObject();
                {
                    // Your play area
                    data.put("action", "ask");
                    data.put(MESSAGE_KEY, dataVO);
                    data.put("count", dataVO);
                    data.put("Date", date);
                    data.put("Name", name);

                    // Put any thing here, and receive it in GcmMessageHandler class
                }


                object.put("data", data);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Headers.Builder headers = new Headers.Builder().add("Authorization", "key=" + API_KEY)
                    .add("Content-Type", JsonRequestBody.CONTENT_TYPE);
            HttpRequest request =
                    new
                            HttpRequest.Builder("https://gcm-http.googleapis.com/gcm/send").requestBody(new JsonRequestBody(object)).headers(headers.build())
                            .build();

            HttpTask.TaskListener listener = new HttpTask.TaskListener<Object>() {
                @Override
                public void onPreExecute() {

                }

                @Override
                public void onPostExecute(ResponseData<String> responseData, Object resultObj) {

                    if (responseData != null && responseData.errorType == ErrorType.SUCCESS) {
                        Toast.makeText(context, "Sent", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                    }
                }
            };

            HttpTask task = new HttpTask.Builder().httpRequest(request).listener(listener).execute();
        } else {
            Toast.makeText(context, "No RegId", Toast.LENGTH_SHORT).show();
        }
    }
}
