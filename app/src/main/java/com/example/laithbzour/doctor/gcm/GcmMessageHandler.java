package com.example.laithbzour.doctor.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.laithbzour.doctor.Notifi_page_2;
import com.example.laithbzour.doctor.R;
import com.google.android.gms.gcm.GcmListenerService;

public class GcmMessageHandler extends GcmListenerService {

    public static final int MESSAGE_NOTIFICATION_ID = 435345;
   public static String Name,Date;
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString(GcmManager.MESSAGE_KEY);
        String action = data.getString("action");
        String app_date = data.getString("Date");
        String p_name = data.getString("Name");
        String count = data.getString("count");
        Name=p_name;
        Date=app_date;
        int f_count=Integer.valueOf(count);
        int type=f_count;
        switch (type)
        {
            case 1:
                createNotification("Title", message);
                break;
            case 2:
                createNotification_2("Title", message);
                break;
            case 3:
                createNotification_3("Title", message);
                break;

        }

    }

    // Creates notification based on title and body received
    private void createNotification(final String title, String body) {
        Context context = getBaseContext();

        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
        int icon = R.drawable.placeholder;
        CharSequence contentTitle = "Appointment";
        CharSequence contentText = "You have appointment from : "+Name;
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, Notifi_page_2.class), 0);
        Notification notification = new Notification.Builder(context)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setSmallIcon(icon)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setContentIntent(contentIntent)
                .build();
        mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, notification);
    }
    private void createNotification_2(final String title, String body) {
        Context context = getBaseContext();

        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
        int icon = R.drawable.delete_button;
        CharSequence contentTitle = "Appointment status";
        CharSequence contentText = "The Appointment Is Rejected";
        Notification notification = new Notification.Builder(context)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setSmallIcon(icon)
                .setDefaults(Notification.DEFAULT_SOUND)
                .build();
        mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, notification);
    }

    private void createNotification_3(final String title, String body) {
        Context context = getBaseContext();

        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
        int icon = R.drawable.check;
        CharSequence contentTitle = "Appointment status";
        CharSequence contentText = "Dr."+ Name +"accept your appointment";
        Notification notification = new Notification.Builder(context)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setSmallIcon(icon)
                .setDefaults(Notification.DEFAULT_SOUND)
                .build();
        mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, notification);
    }
}
