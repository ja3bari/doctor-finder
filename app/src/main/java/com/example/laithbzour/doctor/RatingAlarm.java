package com.example.laithbzour.doctor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class RatingAlarm extends BroadcastReceiver {

    private static final String DEBUG_TAG = "Rating Alarm";

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent rating = new Intent(context, RatingService.class);
        context.startService(rating);
    }
}