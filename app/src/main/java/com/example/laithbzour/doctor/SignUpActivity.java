package com.example.laithbzour.doctor;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableOperationCallback;

import java.io.ByteArrayOutputStream;
import java.util.UUID;

import cn.yangbingqiang.android.parallaxviewpager.ParallaxViewPager;

public class SignUpActivity extends AppCompatActivity {
    private SectionsPagerAdapter mSectionsPagerAdapter;

    public static MobileServiceClient mClient;
    public static MobileServiceTable<user> Doctor_Finder;
    public static MobileServiceTable<master_login> Doctor_Finder_2;
    private ParallaxViewPager mViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        // animation of show activity
        getWindow().getAttributes().windowAnimations=R.anim.push_right;
        //hide the keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // key of windows azure
        try {

            mClient = new MobileServiceClient(
                    "https://docotrfinder.azure-mobile.net/",
                    "OCecmhorpCrBqDmLWZbaTporlbzoKP57",
                    this
            );


        } catch (Exception e) {
            createAndShowDialog(e, "Error");
        }
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ParallaxViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }


        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }
        private ImageView profile_image;
        public static final int RESULT_OK = -1;
        final int TAKE_PICTURE = 1;
        final int ACTIVITY_SELECT_IMAGE = 2;
        private static final String TAG = "SignUp";
        private EditText username, email, password, conf_pass, mobile, age;
        private Button signup_btn;
        private CheckBox show;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_sign_up, container, false);
            profile_image=(ImageView)rootView.findViewById(R.id.btnSelectPhoto_2);
            username = (EditText) rootView.findViewById(R.id.username);
            email = (EditText) rootView.findViewById(R.id.email);
            password = (EditText) rootView.findViewById(R.id.password);
            conf_pass = (EditText) rootView.findViewById(R.id.conf_password);
            mobile = (EditText) rootView.findViewById(R.id.mobile);
            age = (EditText) rootView.findViewById(R.id.age);
            show = (CheckBox) rootView.findViewById(R.id.show_password);
            signup_btn = (Button) rootView.findViewById(R.id.signup_btn);
            show.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(show.isChecked()){
                        password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    }
                    else
                    {
                        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                    }
                }
            });
            signup_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    singUp();
                }
            });
            profile_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectImage();
                }
            });

            return rootView;
        }

        // pick image

        private void selectImage() {
            final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Add Photo!");
            builder.setItems(options, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    if (options[which].equals("Take Photo")) {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, TAKE_PICTURE);
                    } else if (options[which].equals("Choose from Gallery")) {
                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, ACTIVITY_SELECT_IMAGE);
                    } else if (options[which].equals("Cancel")) {
                        dialog.dismiss();
                    }

                }
            });
            builder.show();

        }
        String img;

        @Override
        public void onActivityResult(int requestcode, int resultcode, Intent intent) {
            super.onActivityResult(requestcode, resultcode, intent);
            Context context = getContext();
            if (resultcode == RESULT_OK) {
                if (requestcode == TAKE_PICTURE) {
                    Bitmap photo = (Bitmap) intent.getExtras().get("data");
                    Drawable drawable = new BitmapDrawable(photo);
                    img = bitmapToBase64(photo);
                    profile_image.setImageDrawable(drawable);


                } else if (requestcode == ACTIVITY_SELECT_IMAGE) {
                    Uri selectedImage = intent.getData();
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = context.getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    String picturePath = c.getString(columnIndex);
                    c.close();
                    Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                    Drawable drawable = new BitmapDrawable(thumbnail);
                    profile_image.setImageDrawable(drawable);
                    img = bitmapToBase64(thumbnail);
                    profile_image.setRotation(270);

                }
            }
        }

        public String bitmapToBase64(Bitmap bitmap) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            byte[] byteFormat = stream.toByteArray();
            // get the base 64 string
            String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
            return imgString;
        }

        UUID uuid = UUID.randomUUID();
        String uuidInString = uuid.toString();
        String newid;
        public void singUp() {
            Log.d(TAG, "Register");

            if (!validate()) {
                onSignupFailed();
                return;
            }
            startTask();
        }
        private SignUpTask mSignUpTask;
        private void startTask() {
            cancelTask();
            LocationManager locationManager = (LocationManager)getActivity().getSystemService(LOCATION_SERVICE);

            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            {
                GPSTracker gps;
                gps = new GPSTracker(getActivity());
                final String userName=username.getText().toString();
                final String userEmail=email.getText().toString();
                final String userPassword=password.getText().toString();
                final String userMobile=mobile.getText().toString();
                final String userAge=age.getText().toString();
                final String userImage=img;
                final String userLat=String.valueOf(gps.getLatitude());
                final String userLong=String.valueOf(gps.getLongitude());
                final String Type="user";
                mSignUpTask = new SignUpTask(userName,userEmail,userPassword,
                        userMobile,userAge,userImage,userLat,userLong,Type);
                mSignUpTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
            else
            {
                showGPSDisabledAlertToUser();
            }

        }
        // show dialog to turn on GPS(If GPS Off)
        private void showGPSDisabledAlertToUser(){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                    .setCancelable(false)
                    .setPositiveButton("Close",
                            new DialogInterface.OnClickListener(){
                                public void onClick(DialogInterface dialog, int id){
                                    //close the dialog
                                    dialog.cancel();

                                }
                            });
            alertDialogBuilder.setNegativeButton("GPS Setting",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            // open setting of Gps on Your Phone
                            Intent callGPSSettingIntent = new Intent(
                                    android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(callGPSSettingIntent);
                        }
                    });
            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        }
        private void cancelTask() {

            if (mSignUpTask != null)
                mSignUpTask.cancel(true);
        }

        private class SignUpTask extends AsyncTask {

            ProgressDialog progressDialog;
            final String UserName,UserEmail,UserPassword,UserPhone,UserAge,UserImage,UserLatitude,UserLongitude;
            boolean success;

            public SignUpTask(String UserName, String UserEmail, String UserPassword, String UserPhone,String UserAge,String UserImage
                    ,String UserLatitude,String UserLongitude,String UserCountry) {
                this.UserName=UserName;
                this.UserEmail=UserEmail;
                this.UserPassword=UserPassword;
                this.UserPhone=UserPhone;
                this.UserAge=UserAge;
                this.UserImage=UserImage;
                this.UserLatitude=UserLatitude;
                this.UserLongitude=UserLongitude;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(getContext());
                progressDialog.show();
                progressDialog.setContentView(R.layout.custom_progressdialog);
                progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        cancelTask();
                    }
                });
                try {
                    progressDialog.show();
                } catch (Exception ignored) {
                }
            }

            @Override
            protected Object doInBackground(Object[] objects) {

                SystemClock.sleep(1000);
                if (isCancelled())
                    return null;
                final user item = new user();
                item.mId=uuidInString.toString();

                item.name = UserName;
                item.email = UserEmail;
                item.number = UserPhone;
                item.password = UserPassword;
                item.latitude = UserLatitude;
                item.langitude = UserLongitude;
                item.age = UserAge;
                item.type = String.valueOf("user");
                item.image = img;
                mClient.getTable(user.class).insert(item, new TableOperationCallback<user>() {
                    public void onCompleted(user entity, Exception exception, ServiceFilterResponse response) {
                        if (exception == null) {
                            newid=item.mId.toString();
                            onSignupSuccess();
                            username.setText("");
                            email.setText("");
                            password.setText("");
                            conf_pass.setText("");
                            mobile.setText("");
                            age.setText("");
                            Intent intent = new Intent(getActivity(), Profile.class);
                            intent.putExtra("id", item.mId.toString());
                            startActivity(intent);

                        }
                        else
                        {
                            Toast.makeText(getContext(), "Error !!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                success = true;


                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    SystemClock.sleep(10000);
                } catch (Exception e) {
                }

                if (!success) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "Error !!", Toast.LENGTH_LONG).show();
                }

            }
        }
        public void onSignupSuccess() {
            final master_login items = new master_login();
            items.mId=newid.toString();
            items.m_email=email.getText().toString();
            items.m_password = password.getText().toString();
            items.m_type=String.valueOf("user");

            mClient.getTable(master_login.class).insert(items, new TableOperationCallback<master_login>() {
                @Override
                public void onCompleted(master_login entity, Exception exception, ServiceFilterResponse response) {
                    if (exception == null) {
                        Toast.makeText(getContext(), "Register Success", Toast.LENGTH_LONG).show();
                        cancelTask();
                    } else {

                        onSignupFailed();

                    }
                }
            });


        }
        public void onSignupFailed() {
            Toast.makeText(getContext(), "Error !!", Toast.LENGTH_LONG).show();
        }
        public boolean validate() {
            boolean valid = true;
            String name = username.getText().toString();
            String email_user = email.getText().toString();
            String user_password = password.getText().toString();
            String mobile_user = mobile.getText().toString();
            String user_age = age.getText().toString();
            String con_password= conf_pass.getText().toString();

            if (name.isEmpty() || name.length() < 3) {
                username.setError("Enter Valid Name");
                username.setText("");
                valid = false;
            } else {
                username.setError(null);
            }

            if (email_user.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email_user).matches()) {
                email.setError("Enter Valid Email");
                email.setText("");
                valid = false;
            } else {
                email.setError(null);
            }

            if (user_password.isEmpty() || password.length() < 8) {
                password.setError("Enter Password Between 8 and 10 Characters");
                password.setText("");
                valid = false;
            } else {
                password.setError(null);
            }
            if (con_password.isEmpty()|| !con_password.equals(user_password)) {
                conf_pass.setError("Password not match");
                conf_pass.setText("");
                valid = false;
            } else {
                conf_pass.setError(null);
            }

            if (mobile_user.isEmpty() || mobile_user.length() < 10) {
                mobile.setError("Enter Valid Mobile Number");
                mobile.setText("");
                valid = false;
            } else {
                mobile.setError(null);
            }
            if (user_age.isEmpty() || user_age.length() > 3) {
                age.setError("Enter Valid Value");
                age.setText("");
                valid = false;
            }
            else
            {
                age.setError(null);
            }

            return valid;
        }
        @Override
        public void onDestroy() {
            cancelTask();
            super.onDestroy();
        }
    }



    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return PlaceholderFragment.newInstance(position + 1);

                case 1:
                    SignUp_Doctor tab2 = new SignUp_Doctor();
                    return tab2;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Patient";
                case 1:
                    return "Doctor";

            }
            return null;
        }
    }
    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if (exception.getCause() != null) {
            ex = exception.getCause();
        }
        createAndShowDialog(ex.getMessage(), title);
    }

    private void createAndShowDialog(final String message, final String title) {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);

        builder.setMessage(message);
        builder.setTitle(title);
        builder.create().show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
