package com.example.laithbzour.doctor.utils;

import android.app.Application;

public class AppInstance extends Application {

    private static AppInstance sInstance;

    public static AppInstance getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        sInstance = this;
        super.onCreate();
    }
}
