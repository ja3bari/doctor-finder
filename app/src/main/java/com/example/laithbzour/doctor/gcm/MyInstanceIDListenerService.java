package com.example.laithbzour.doctor.gcm;

import com.example.laithbzour.doctor.utils.AppPreferencesManager;
import com.google.android.gms.iid.InstanceIDListenerService;

public class MyInstanceIDListenerService extends InstanceIDListenerService {

    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify of changes
        AppPreferencesManager.getInstance(this).removeValue(RegistrationIntentService.TOKEN);
        RegistrationIntentService.startService(this);
    }
}
