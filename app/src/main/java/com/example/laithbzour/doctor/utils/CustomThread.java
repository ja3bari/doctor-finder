package com.example.laithbzour.doctor.utils;

import android.os.Handler;
import android.os.Looper;

/**
 * Deals like
 *
 * @author m.jorban
 */
@SuppressWarnings("unchecked")
public abstract class CustomThread extends Thread {

    private Handler uiHandler;
    private boolean cancelled;
    private boolean running;

    /**
     * Constructor must called from UI thread
     */
    public CustomThread() {

        uiHandler = new Handler(Looper.getMainLooper());
    }

    /**
     * Empty .. left to the childs
     *
     * @see android.os.AsyncTask#onPreExecute
     */
    protected void onPreExecute() {

        // Left to the childs
    }

    /**
     * Empty .. left to the childs
     *
     * @param <Progress>
     * @see android.os.AsyncTask#onProgressUpdate
     */
    protected <Progress> void onProgressUpdate(Progress... values) {

        // Left to the childs
    }

    /**
     * Empty .. left to the childs
     *
     * @param <Progress>
     * @see android.os.AsyncTask#publishProgress
     */
    @SafeVarargs
    protected final <Progress> void publishProgress(final Progress... values) {

        runRunnable(new Runnable() {

            @Override
            public void run() {

                if (!isCancelled())
                    onProgressUpdate(values);
            }
        });
    }

    /**
     * @see android.os.AsyncTask#doInBackground
     */
    protected abstract void doInBackground();

    /**
     * Empty .. left to the childs
     *
     * @see android.os.AsyncTask#onPostExecute
     */
    protected void onPostExecute() {

        // Left to the childs
    }

    @Override
    public void run() {
        super.run();

        doInBackground();

        boolean ran = runRunnable(new Runnable() {

            @Override
            public void run() {

                if (!isCancelled())
                    onPostExecute();
                setRunning(false);
            }
        });

        if (!ran)
            setRunning(false);
    }

    private boolean isValidHandler() {

        return uiHandler.getLooper() == Looper.getMainLooper();
    }

    private boolean runRunnable(Runnable run) {

        boolean valid = isValidHandler();
        if (valid)
            uiHandler.post(run);
        return valid;
    }

    public void cancel() {

        cancelled = true;
    }

    public boolean isCancelled() {

        return cancelled;
    }

    public boolean isRunning() {
        return running && !isCancelled();
    }

    private void setRunning(boolean running) {
        this.running = running;
    }


    @Override
    public void start() {

        setRunning(true);
        onPreExecute();
        super.start();
    }

    /**
     * Cancel this task if it's running.
     *
     * @param customThread {@link CustomThread} can be <code>null</code>
     */
    public static final void cancelTask(CustomThread customThread) {

        if (customThread != null)
            customThread.cancel();
    }

    /**
     * Check whether this task is running and not cancelled
     *
     * @param customThread {@link CustomThread} can be <code>null</code>
     * @return <code>true</code> if this task is running and not cancelled, otherwise <code>false</code>
     */
    public static final boolean isRunning(CustomThread customThread) {

        if (customThread != null)
            return customThread.isRunning();
        return false;
    }
}
