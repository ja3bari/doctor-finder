package com.example.laithbzour.doctor.utils.http;

public class ResponseData<T> {

	public T result;
	public ErrorType errorType;

	public ResponseData() {
		this(ErrorType.GENERAL);
	}

	public ResponseData(ErrorType errorType) {
		this.errorType = errorType;
	}
}
