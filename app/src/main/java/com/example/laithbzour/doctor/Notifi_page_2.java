package com.example.laithbzour.doctor;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.laithbzour.doctor.gcm.GcmManager;
import com.example.laithbzour.doctor.gcm.GcmMessageHandler;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableOperationCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Notifi_page_2 extends AppCompatActivity {
    public static MobileServiceClient mClient;
    public static MobileServiceTable<appointment> Doctor_Finder;
    private MobileServiceList<appointment> result;
    public static final String TAG = "NOTIFY";
    private TextView date_1, time_1,text;
    private Button Accept, Reject;
    String dv_id;
    String results, result2, mydate, result3, result4,result5,result6,result7,dr_name;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_3);
        try {

            mClient = new MobileServiceClient(
                    "https://docotrfinder.azure-mobile.net/",
                    "OCecmhorpCrBqDmLWZbaTporlbzoKP57",
                    this
            );

        } catch (Exception e) {
            createAndShowDialog(e, "Error");
        }
        date_1 = (TextView) findViewById(R.id.date);
        time_1 = (TextView) findViewById(R.id.time);
        Accept = (Button) findViewById(R.id.accept);
        Reject = (Button) findViewById(R.id.reject);
        text=(TextView)findViewById(R.id.task_activity2);
        result6=GcmMessageHandler.Name;
        dr_name=Show_dr.name_dr.toString();
        text.setText(result6+"needs to make an appointment on");
        result7=GcmMessageHandler.Date;
        mydate = result7;
        date_1.setText(result7);
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("HH:mm a");
        date.setTimeZone(TimeZone.getTimeZone("GMT+1:00"));
        String localTime = date.format(currentLocalTime);
        time_1.setText(localTime);

        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
        int id = GcmMessageHandler.MESSAGE_NOTIFICATION_ID;
        Log.i(TAG, "Notification: ID=" + id + " canceled");
        mNotificationManager.cancel(id);
        results = PreferenceManager.getDefaultSharedPreferences(this).getString("ID_dr", "defaultStringIfNothingFound");
        result2 = PreferenceManager.getDefaultSharedPreferences(this).getString("ID_User", "defaultStringIfNothingFound");
        result3 = PreferenceManager.getDefaultSharedPreferences(this).getString("User_Device_id", "defaultStringIfNothingFound");
        result4 = PreferenceManager.getDefaultSharedPreferences(this).getString("user_name", "defaultStringIfNothingFound");
        result5 = PreferenceManager.getDefaultSharedPreferences(this).getString("date", "defaultStringIfNothingFound");

        dv_id = result3;
        Accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Update_item();
                GcmManager.sendPushMessage(Notifi_page_2.this, dv_id, 3,"",dr_name);

            }
        });

        Reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GcmManager.sendPushMessage(Notifi_page_2.this, dv_id, 2,"","");
                finish();

            }
        });
    }

    public void Update_item() {

        startTask();
    }

    private Rating_pTask mRatingTaskk;

    private void startTask() {
        cancelTask();

        final String Dr_id = results;
        final String User_id = result2;

        mRatingTaskk = new Rating_pTask(Dr_id, User_id);
        mRatingTaskk.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void cancelTask() {

        if (mRatingTaskk != null)
            mRatingTaskk.cancel(true);
    }

    private class Rating_pTask extends AsyncTask {

        ProgressDialog progressDialog;
        final String Dr_id, User_id;
        boolean success;

        public Rating_pTask(String Dr_id, String User_id) {
            this.Dr_id = Dr_id;
            this.User_id = User_id;


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Notifi_page_2.this);
            progressDialog.show();
            progressDialog.setContentView(R.layout.custom_progressdialog);
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    cancelTask();
                }
            });
            try {
                progressDialog.show();
            } catch (Exception ignored) {
            }
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            if (isCancelled())
                return null;
            final appointment item = new appointment();
            item.user_id = User_id;
            item.doctor_id = Dr_id;
            item.date_time = result7;
            item.status = "accept";
            item.paitent_name = result4.toString();

            mClient.getTable(appointment.class).insert(item, new TableOperationCallback<appointment>() {
                public void onCompleted(appointment entity, Exception exception, ServiceFilterResponse response) {
                    if (exception == null) {
                        progressDialog.dismiss();
                        Toast.makeText(Notifi_page_2.this, "The Appointment Added", Toast.LENGTH_LONG).show();
                        finish();

                    } else {
                        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
                    }
                }
            });


            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            try {
                SharedPreferences preferences = getSharedPreferences("ID_Craft", 0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();


            } catch (Exception e) {
            }

        }

    }
    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if (exception.getCause() != null) {
            ex = exception.getCause();
        }
        createAndShowDialog(ex.getMessage(), title);
    }

    private void createAndShowDialog(final String message, final String title) {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);

        builder.setMessage(message);
        builder.setTitle(title);
        builder.create().show();
    }
}
