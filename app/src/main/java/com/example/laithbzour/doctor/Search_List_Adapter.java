package com.example.laithbzour.doctor;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class Search_List_Adapter extends ArrayAdapter<String>{

    private final Activity context;
    ArrayList<String> name = new ArrayList<String>();
    ArrayList<String> date = new ArrayList<String>();
    ArrayList<String> time = new ArrayList<String>();

    public Search_List_Adapter(Activity context,
                               ArrayList<String> name, ArrayList<String> date, ArrayList<String> time) {
        super(context, R.layout.list_of_search, name);
        this.context = context;
        this.name = name;
        this.date = date;
        this.time = time;



    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.list_of_search, null, false);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.craft_name);
        txtTitle.setText(name.get(position));
        TextView txtTitle_1 = (TextView) rowView.findViewById(R.id.date);
        txtTitle_1.setText(date.get(position));
        TextView txtTitle_2 = (TextView) rowView.findViewById(R.id.time);
        txtTitle_2.setText(time.get(position));




        return rowView;
    }
}