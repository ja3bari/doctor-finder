package com.example.laithbzour.doctor;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableOperationCallback;

public class RatingPage extends AppCompatActivity {
    public static MobileServiceClient mClient;
    public MobileServiceTable<doctor> doctor_finder;
    public MobileServiceTable<doctor_rating> doctor_finder_2;
    public MobileServiceList<doctor_rating> result;
    private Button rating;
    private ImageView imgProfile;
    ProgressDialog progressDialog;
    String results,result2;
    RatingBar Ratig_value;
    TextView dr_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_page);
        try {

            mClient = new MobileServiceClient(
                    "https://docotrfinder.azure-mobile.net/",
                    "OCecmhorpCrBqDmLWZbaTporlbzoKP57",
                    this
            );

        } catch (Exception e) {
            createAndShowDialog(e, "Error");
        }
        rating= (Button)findViewById(R.id.rating_btn);
        imgProfile=(ImageView)findViewById(R.id.profile_image);
        Ratig_value = (RatingBar) findViewById(R.id.ratingBar);
        dr_name = (TextView) findViewById(R.id.name);

        rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Update_item();

            }
        });

        results= PreferenceManager.getDefaultSharedPreferences(this).getString("ID_dr", "defaultStringIfNothingFound");
        result2= PreferenceManager.getDefaultSharedPreferences(this).getString("ID_User", "defaultStringIfNothingFound");
        run();


    }


    String image_profile,name_1;
    public void  run(){

        doctor_finder = mClient.getTable(doctor.class);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final MobileServiceList<doctor> result =
                            doctor_finder.where().field("id").eq(results).execute().get();
                    for (final doctor item : result) {
                        image_profile=item.image.toString();
                        name_1=item.name;

                    }

                } catch (Exception exception) {
                    exception.printStackTrace();
                }

                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        dr_name.setText("Dr."+name_1);

                        if (image_profile==null) {
                            imgProfile.setImageResource(R.drawable.doctor_img);

                        } else
                        {
                            Bitmap img=base64ToBitmap(image_profile);
                            Drawable drawable = new BitmapDrawable(img);
                            imgProfile.setImageDrawable(drawable);
                            imgProfile.setRotation(270);

                        }


                    }
                });
            }
        }.execute();

    }

    public void  Update_item(){

        startTask();
    }
    private Rating_pTask mRatingTaskk;
    private void startTask() {
        cancelTask();

        final int rating= (int) Ratig_value.getRating();
        mRatingTaskk = new Rating_pTask(rating);
        mRatingTaskk.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
    private void cancelTask() {

        if (mRatingTaskk != null)
            mRatingTaskk.cancel(true);
    }
    private class Rating_pTask extends AsyncTask {

        ProgressDialog progressDialog;
        final Integer rating;
        boolean success;

        public Rating_pTask(int rating) {
            this.rating = rating;


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(RatingPage.this);
            progressDialog.show();
            progressDialog.setContentView(R.layout.custom_progressdialog);
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    cancelTask();
                }
            });
            try {
                progressDialog.show();
            } catch (Exception ignored) {
            }
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            if (isCancelled())
                return null;
            final doctor_rating item = new doctor_rating();
            item.paitent_id = result2;
            item.dr_id = results;
            item.rating_value = String.valueOf(rating);
            mClient.getTable(doctor_rating.class).insert(item, new TableOperationCallback<doctor_rating>() {
                public void onCompleted(doctor_rating entity, Exception exception, ServiceFilterResponse response) {
                    if (exception == null) {
                        progressDialog.dismiss();
                        Toast.makeText(RatingPage.this, "Thanx For Rating", Toast.LENGTH_LONG).show();
                        System.exit(1);


                    } else {
                        Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();
                    }
                }
            });


            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            try {
                SharedPreferences preferences = getSharedPreferences("ID_dr", 0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();


            } catch (Exception e) {
            }

        }


    }

    private Bitmap base64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }


    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if(exception.getCause() != null){
            ex = exception.getCause();
        }
        createAndShowDialog(ex.getMessage(), title);
    }
    private void createAndShowDialog(final String message, final String title) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(message);
        builder.setTitle(title);
        builder.create().show();
    }
}
