package com.example.laithbzour.doctor.utils.http;

import android.text.TextUtils;

import com.example.laithbzour.doctor.utils.CustomThread;


/**
 * Use this class when you need to call {@link HttpRequest} in a non UI thread
 *
 * @param <T>
 * @see HttpRequest
 */
@SuppressWarnings("unchecked")
public final class HttpTask<T> extends CustomThread {

    private final HttpRequest mHttpRequest;
    private final ResponseParser mResponseParser;
    private final TaskListener mListener;
    private final String mResponseJsonKey;

    private ResponseData<String> mResponseData;
    private T mResultObj;

    private HttpTask(HttpRequest httpRequest, ResponseParser responseParser, TaskListener listener, String responseJsonKey) {

        mHttpRequest = httpRequest;
        mResponseParser = responseParser;
        mListener = listener;
        mResponseJsonKey = responseJsonKey;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (mListener != null)
            mListener.onPreExecute();
    }

    @Override
    protected void doInBackground() {

        if (TextUtils.isEmpty(mHttpRequest.url))
            return;
        mResponseData = mHttpRequest.execute();
    }

    @Override
    protected void onPostExecute() {
        super.onPostExecute();
        if (mListener != null)
            mListener.onPostExecute(mResponseData, mResultObj);
    }

    @Override
    public void cancel() {
        super.cancel();
        mHttpRequest.cancel();
    }

    /**
     * Start the Task
     */
    public void execute() {
        start();
    }

    /*****************************************/
    /*************** BUILDER *****************/
    /*****************************************/
    public static final class Builder {

        private HttpRequest httpRequest;
        private ResponseParser responseParser;
        private TaskListener listener;

        public Builder() {
        }

        /**
         * Use {@link HttpRequest.Builder} to prepare {@link HttpRequest}
         *
         * @param httpRequest {@link HttpRequest}
         * @return
         */
        public Builder httpRequest(HttpRequest httpRequest) {
            this.httpRequest = httpRequest;
            return this;
        }

        /**
         * Use this if you need to add custom response parser
         *
         * @param responseParser {@link ResponseParser}
         * @return
         */
        public Builder responseParser(ResponseParser responseParser) {
            this.responseParser = responseParser;
            return this;
        }

        /**
         * @param listener {@link TaskListener}
         * @return
         */
        public Builder listener(TaskListener listener) {
            this.listener = listener;
            return this;
        }

        /**
         * Build the task
         *
         * @return
         * @see #execute()
         */
        public HttpTask build() {
            return new HttpTask<>(httpRequest, responseParser, listener, null);
        }

        /**
         * Build the task then execute it
         *
         * @see #build()
         */
        public HttpTask execute() {
            HttpTask task = build();
            task.execute();
            return task;
        }
    }

    public interface TaskListener<T> {

        void onPreExecute();

        void onPostExecute(ResponseData<String> responseData, T resultObj);
    }

    /**
     * Add your own implementation for parsing the returned data from the request
     *
     * @param <T>
     * @author m.jorban
     */
    public interface ResponseParser<T> {

        T parse(String result);
    }
}
