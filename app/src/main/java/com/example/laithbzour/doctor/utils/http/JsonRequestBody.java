package com.example.laithbzour.doctor.utils.http;

import android.text.TextUtils;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.util.concurrent.atomic.AtomicBoolean;

public class JsonRequestBody implements GenericRequestBody {

	public static final String CONTENT_TYPE = "application/json; charset=utf-8";
	private final String jsonText;

	public JsonRequestBody(JSONObject jsonObject) {
		this(jsonObject == null ? "" : jsonObject.toString());
	}

	public JsonRequestBody(String jsonText) {
		this.jsonText = jsonText;
	}

	@Override
	public void post(HttpURLConnection connection, AtomicBoolean isCancelled) throws IOException {
		if (connection == null || TextUtils.isEmpty(jsonText))
			return;

		connection.setRequestProperty("Content-Type", CONTENT_TYPE);
		Writer outputStream = null;
		try {
			outputStream = new OutputStreamWriter(connection.getOutputStream());
			if (!isCancelled(isCancelled)) {
				outputStream.write(jsonText);
				outputStream.flush();
			}
		}
		finally {
		}
	}

	@Override
	public RequestBody prepareRequestBody(AtomicBoolean isCancelled) {
		if (TextUtils.isEmpty(jsonText))
			return null;

		return RequestBody.create(MediaType.parse(CONTENT_TYPE), jsonText);
	}

	private boolean isCancelled(AtomicBoolean isCancelled) {
		return isCancelled != null && isCancelled.get();
	}
}
