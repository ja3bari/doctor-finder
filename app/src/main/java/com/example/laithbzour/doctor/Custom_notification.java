package com.example.laithbzour.doctor;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import com.example.laithbzour.doctor.gcm.GcmManager;

public class Custom_notification extends AppCompatActivity {
Button send;
    DatePicker datePicker;
    String id,u_id,name,Date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_notification);
        send=(Button)findViewById(R.id.positive_button);
        datePicker=(DatePicker)findViewById(R.id.date_picker);
        Integer dobYear = datePicker.getYear();
        Integer dobMonth = datePicker.getMonth();
        Integer dobDate = datePicker.getDayOfMonth();
        StringBuilder sb=new StringBuilder();
        sb.append(dobYear.toString()).
                append("-").append(dobMonth.toString()).append("-").append(dobDate.toString())
                .append("");
        String dobStr=sb.toString();
        String selectedDate = dobStr;
         Date = selectedDate;
        name=Profile.user_name.toString();
        id=Show_dr.ids.toString();
        u_id=Show_dr.user_id.toString();
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GcmManager.sendPushMessage(Custom_notification.this, id, 1,Date,name);

            }
        });


    }
}
