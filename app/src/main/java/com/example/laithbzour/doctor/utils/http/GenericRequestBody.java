package com.example.laithbzour.doctor.utils.http;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.RequestBody;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.concurrent.atomic.AtomicBoolean;

public interface GenericRequestBody {

	/**
	 * Implement here the logic for posting data using {@link HttpURLConnection}
	 *
	 * @param connection
	 * @param isCancelled
	 *            an {@link AtomicBoolean} object that holding any change in cancel state from the calling class
	 * @throws IOException
	 */
	void post(HttpURLConnection connection, AtomicBoolean isCancelled) throws IOException;

	/**
	 * Use this method for preparing {@link RequestBody} in case you are using {@link OkHttpClient}
	 * 
	 * @param isCancelled
	 *            an {@link AtomicBoolean} object that holding any change in cancel state from the calling class
	 * @return {@link RequestBody}
	 */
	RequestBody prepareRequestBody(AtomicBoolean isCancelled);
}
