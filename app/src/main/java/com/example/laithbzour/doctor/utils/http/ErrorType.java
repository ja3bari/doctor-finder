package com.example.laithbzour.doctor.utils.http;

public enum ErrorType {

	SUCCESS, NETWORK, GENERAL;
}
