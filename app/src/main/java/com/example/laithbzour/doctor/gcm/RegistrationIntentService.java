package com.example.laithbzour.doctor.gcm;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.laithbzour.doctor.R;
import com.example.laithbzour.doctor.utils.AppPreferencesManager;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

public class RegistrationIntentService extends IntentService {

    public static final String TOKEN = "GoogleDeviceId";

    // abbreviated tag name
    private static final String TAG = "RegIntentService";

    public RegistrationIntentService() {
        super(TAG);
    }

    public static void startService(Context context) {
        Intent intent = new Intent(context, RegistrationIntentService.class);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Make a call to Instance API
        InstanceID instanceID = InstanceID.getInstance(this);
        String senderId = getResources().getString(R.string.gcm_defaultSenderId);
        try {
            // request token that will be used by the server to send push notifications
            final String token = instanceID.getToken(senderId, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
            Log.e("TOKEN", "GCM Registration Token: " + token);

            String value = AppPreferencesManager.getInstance(this).getStringValue(TOKEN, null);
            if (token != null && !token.equals(value)) {
                // pass along this data
                sendRegistrationToServer(token);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this.getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void sendRegistrationToServer(String token) {
        // TODO
        // send network request(update the server)
        // Add custom implementation, as needed.
        AppPreferencesManager.getInstance(this).setStringValue(TOKEN, token);
    }
}
