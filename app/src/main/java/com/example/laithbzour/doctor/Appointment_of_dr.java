package com.example.laithbzour.doctor;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;

import java.util.ArrayList;

public class Appointment_of_dr extends Fragment {
    public static MobileServiceClient mClient;
    public MobileServiceTable<appointment> doctor_finder_2;
    public MobileServiceTable<user> doctor_finder;
    private MobileServiceList<appointment> result;
    private MobileServiceList<user> results;
    private Search_List_Adapter_2 adapter;
    ListView list;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dr_appointment, container, false);

        try {

            mClient = new MobileServiceClient(
                    "https://docotrfinder.azure-mobile.net/",
                    "OCecmhorpCrBqDmLWZbaTporlbzoKP57",
                    getActivity()
            );


        } catch (Exception e) {
            createAndShowDialog(e, "Error");
        }

        list = (ListView) rootView.findViewById(R.id.list_favo);
        run_2();

        return rootView;
    }
    String date;
    public void  run_2(){

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final String user_id=Profile.user_id.toString();
                    result = mClient.getTable(appointment.class).where().field("user_id").eq(user_id)
                            .execute().get();
                    if(result!=null)
                    {    for (final appointment item : result) {

                        String user_Id=item.user_id.toString();
                        date=item.date_time.toString();
                        results = mClient.getTable(user.class).where().field("id").eq(user_Id)
                                .execute().get();
                    }
                    }


                } catch (Exception exception) {
                    exception.printStackTrace();
                }

                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(results!=null)
                        {
                            ArrayList<String> name = new ArrayList<String>();
                            ArrayList<String> Date = new ArrayList<String>();
                            final ArrayList<String> user_id = new ArrayList<String>();
                            for (final user item : results) {
                                user_id.add(item.mId);
                                name.add(item.name);
                                Date.add(date);

                            }
                            // set item of list in adapter
                            adapter = new Search_List_Adapter_2(getActivity(), name, Date);
                            list.setAdapter(adapter);
                        }




                        }

                });
            }
        }.execute();

    }


    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if (exception.getCause() != null) {
            ex = exception.getCause();
        }
        createAndShowDialog(ex.getMessage(), title);
    }

    private void createAndShowDialog(final String message, final String title) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(message);
        builder.setTitle(title);
        builder.create().show();
    }


}



