package com.example.laithbzour.doctor;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.laithbzour.doctor.gcm.GcmManager;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;

public class LoginActivity extends AppCompatActivity {
 private TextView signup_link;
    private Button login;
    private EditText Email, Password;
    public static MobileServiceClient mClient;
    public MobileServiceTable<master_login> salehli;
    private static final String PREFS_NAME = "preferences";
    public static final String PREF_UNAME = "Username";
    public static final String PREF_PASSWORD = "Password";
    private String UnameValue;
    private String PasswordValue;
    private static final String TAG = "Login";
    private static final int REQUEST_login = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().getAttributes().windowAnimations=R.anim.push_right;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        signup_link=(TextView)findViewById(R.id.signup_link);
        login=(Button)findViewById(R.id.login_btn);

        GcmManager.startGCM(this);

        try {

            mClient = new MobileServiceClient(
                    "https://docotrfinder.azure-mobile.net/",
                    "OCecmhorpCrBqDmLWZbaTporlbzoKP57",
                    this
            );


        } catch (Exception e) {
            createAndShowDialog(e, "Error");
        }

        Email = (EditText) findViewById(R.id.email_m);
        Password = (EditText) findViewById(R.id.password);

        signup_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,SignUpActivity.class);
                startActivity(intent);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();

            }
        });
        getSoftwareVersion();
    }


    private String getSoftwareVersion() {
        PackageInfo pi;
        try {
            pi = getPackageManager().getPackageInfo(getPackageName(), 0);
            Log.d(TAG, String.valueOf(pi.versionName));

            return pi.versionName;

        } catch (final PackageManager.NameNotFoundException e) {
            return "na";
        }
    }
    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }
        startTask();
    }

    private LoginTask mLoginTask;

    private void startTask() {
        cancelTask();
        final String email_master = Email.getText().toString();
        final String password_master = Password.getText().toString();
        mLoginTask = new LoginTask(email_master, password_master);
        mLoginTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void cancelTask() {

        if (mLoginTask != null)
            mLoginTask.cancel(true);
    }

    private class LoginTask extends AsyncTask {

        ProgressDialog progressDialog;
        private MobileServiceList<master_login> result;
        final String email, password;
        boolean success;

        public LoginTask(String email, String password) {
            this.email = email;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.show();
            progressDialog.setContentView(R.layout.custom_progressdialog);
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    cancelTask();
                }
            });
            try {
                progressDialog.show();
            } catch (Exception ignored) {
            }
        }

        @Override
        protected Object doInBackground(Object[] objects) {

            SystemClock.sleep(3000);
            if (isCancelled())
                return null;
            try {
                result = mClient.getTable(master_login.class).where().field("email").eq(email)
                        .and().field("password").eq(password).execute().get();
                success = true;
            } catch (Exception exception) {
                return exception;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
            }

            if (!success) {
                Toast.makeText(getBaseContext(), "Incorrect Email & Password", Toast.LENGTH_LONG).show();
            }
            if (result == null) {
                Toast.makeText(getBaseContext(),"Incorrect Email & Password", Toast.LENGTH_LONG).show();
                return;
            }

            for (master_login item : result) {
                String type = item.m_type.toString();

                String value = type;
                if ("user".equals(value)) {
                    SharedPreferences settings = getSharedPreferences(PREFS_NAME,
                            Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = settings.edit();

                    UnameValue = Email.getText().toString();
                    PasswordValue = Password.getText().toString();
                    editor.putString(PREF_UNAME, UnameValue);
                    editor.putString(PREF_PASSWORD, PasswordValue);
                    editor.commit();
                    Email.setText("");
                    Password.setText("");
                    Intent intent = new Intent(LoginActivity.this, Profile.class);
                    intent.putExtra("id", item.mId.toString());
                    startActivity(intent);
                    finish();
                    return;
                }
                else if ("doctor".equals(value)) {
                    SharedPreferences settings = getSharedPreferences(PREFS_NAME,
                            Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = settings.edit();

                    UnameValue = Email.getText().toString();
                    PasswordValue = Password.getText().toString();
                    editor.putString(PREF_UNAME, UnameValue);
                    editor.putString(PREF_PASSWORD, PasswordValue);
                    editor.commit();
                    Email.setText("");
                    Password.setText("");
                    Intent intent = new Intent(LoginActivity.this, DoctorProfile.class);
                    intent.putExtra("id", item.mId.toString());
                    startActivity(intent);
                    finish();
                    return;
                }
            }
            Toast.makeText(getBaseContext(), "Incorrect Email & Password !!", Toast.LENGTH_LONG).show();
            Password.setText("");
        }
    }
    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Incorrect Email & Password !!", Toast.LENGTH_LONG).show();
        Password.setText("");

    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }


    public boolean validate() {
        boolean valid = true;

        String email = Email.getText().toString();
        String password = Password.getText().toString();

        if (email.isEmpty() ) {
            Email.setError("Enrer Valid Email");
            Email.setText("");
            valid = false;
        } else {
            Email.setError(null);
        }

        if (password.isEmpty()) {
            Password.setError("Enter Valid Password");
            Password.setText("");
            valid = false;
        } else {
            Password.setError(null);
        }

        return valid;
    }

    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if (exception.getCause() != null) {
            ex = exception.getCause();
        }
        createAndShowDialog(ex.getMessage(), title);
    }

    private void createAndShowDialog(final String message, final String title) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(message);
        builder.setTitle(title);
        builder.create().show();
    }
}
