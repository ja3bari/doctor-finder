
package com.example.laithbzour.doctor;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Update_Profile_Doctor extends Fragment {
    public static MobileServiceClient mClient;
    public MobileServiceTable<doctor> doctors;
    private EditText new_username,new_pass,new_phone,new_age,conf_pass;
    private Button update_data;
    final int TAKE_PICTURE = 1;
    final int ACTIVITY_SELECT_IMAGE = 2;
    private ImageView pick_img;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_update_doctor, container, false);
        getActivity().getWindow().getAttributes().windowAnimations=R.anim.push_left;
        getActivity(). getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        try {

            mClient = new MobileServiceClient(
                    "https://docotrfinder.azure-mobile.net/",
                    "OCecmhorpCrBqDmLWZbaTporlbzoKP57",
                    getActivity()
            );

        } catch (Exception e) {
            createAndShowDialog(e, "Error");
        }

        new_username=(EditText)rootView.findViewById(R.id.username);
        new_pass=(EditText)rootView.findViewById(R.id.password);
        new_phone=(EditText)rootView.findViewById(R.id.mobile);
        new_age=(EditText)rootView.findViewById(R.id.age);
        update_data=(Button)rootView.findViewById(R.id.update);
        conf_pass=(EditText)rootView.findViewById(R.id.conf_password);
        pick_img=(ImageView)rootView.findViewById(R.id.btnSelectPhoto_2);


        pick_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // selectImage();

            }
        });
        update_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Update_item();

            }
        });
        return rootView;
    }

    String img;
    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                if (options[which].equals("Take Photo")) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, TAKE_PICTURE);
                } else if (options[which].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, ACTIVITY_SELECT_IMAGE);
                } else if (options[which].equals("Cancel")) {
                    dialog.dismiss();
                }

            }
        });
        builder.show();

    }

    @Override
    public void onActivityResult(int requestcode, int resultcode, Intent intent) {
        super.onActivityResult(requestcode, resultcode, intent);
        final int RESULT_OK = -1;
        Context context = getContext();
        if (resultcode == RESULT_OK) {
            if (requestcode == TAKE_PICTURE) {
                Bitmap photo = (Bitmap) intent.getExtras().get("data");
                Drawable drawable = new BitmapDrawable(photo);
                pick_img.setImageDrawable(drawable);
                img = bitmapToBase64(photo);

            } else if (requestcode == ACTIVITY_SELECT_IMAGE) {
                Uri selectedImage = intent.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = context.getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                Drawable drawable = new BitmapDrawable(thumbnail);
                pick_img.setImageDrawable(drawable);
                img = bitmapToBase64(thumbnail);

            }
        }
    }

    public String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }
    ProgressDialog progressDialog;

    public void  Update_item(){
        if (!validate()) {
            onUpdateFalid();
            return;
        }
        progressDialog = new ProgressDialog(getContext());
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progressdialog);
        final String name=new_username.getText().toString();
        final String pass=new_pass.getText().toString();
        final String phone=new_phone.getText().toString();
        final String age=new_age.getText().toString();

        doctors = mClient.getTable(doctor.class);
        new AsyncTask<Void, Void, Void>() {
            final String Data = Profile.user_id.toString();
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final MobileServiceList<doctor> result =
                            doctors.where().field("id").eq(Data).execute().get();
                    for (final doctor item : result) {
                        Log.v("Name Of User:", item.name);
                        item.name=name;
                        item.password=pass;
                        item.mobile_number=phone;
                        item.image=img;
                        doctors.update(item).get();
                        progressDialog.dismiss();

                    }

                } catch (Exception exception)
                {
                    exception.printStackTrace();
                    progressDialog.dismiss();
                }

                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new_username.setText("");
                        new_pass.setText("");
                        conf_pass.setText("");
                        new_phone.setText("");
                        Toast.makeText(getActivity(), "Update", Toast.LENGTH_LONG).show();
                        getActivity().finish();

                    }
                });
            }
        }.execute();

    }

    public void onUpdateFalid()
    {
        Toast.makeText(getActivity(), "Error !!", Toast.LENGTH_LONG).show();

    }
    public boolean validate() {
        boolean valid = true;
        String name = new_username.getText().toString();
        String user_password = new_pass.getText().toString();
        String mobile_user = new_phone.getText().toString();
        String user_age = new_age.getText().toString();
        String conf_passwpord = conf_pass.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            new_username.setError("Enter Valid Name");
            valid = false;
        } else {
            new_username.setError(null);
        }

        if (user_password.isEmpty() || new_pass.length() < 8) {
            new_pass.setError("Enter Password between 8 and 10 characters");
            valid = false;
        } else {
            new_pass.setError(null);
        }
        if (conf_passwpord.isEmpty() || new_pass.getText().toString().matches(conf_pass.getText().toString())) {
            conf_pass.setError("Enter Valid password ");
            valid = false;
        } else {
            conf_pass.setError(null);
        }
        if (mobile_user.isEmpty() || mobile_user.length() < 10) {
            new_phone.setError("Enter Valid Phone");
            valid = false;
        } else {
            new_phone.setError(null);
        }
        if (user_age.isEmpty() || user_age.length() > 3) {
            new_age.setError("Enter Valid Value");
            valid = false;
        }
        else
        {
            new_age.setError(null);
        }

        return valid;
    }
    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if (exception.getCause() != null) {
            ex = exception.getCause();
        }
        createAndShowDialog(ex.getMessage(), title);
    }

    private void createAndShowDialog(final String message, final String title) {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());

        builder.setMessage(message);
        builder.setTitle(title);
        builder.create().show();
    }

}



