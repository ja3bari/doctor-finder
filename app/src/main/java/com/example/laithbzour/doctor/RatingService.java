package com.example.laithbzour.doctor;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class RatingService extends Service
	{
	
  private Looper mServiceLooper;
  private ServiceHandler mServiceHandler;
  private final String DEBUG_TAG = "Rating::Service";
  private String best;
		Context context;
  private final class ServiceHandler extends Handler {
      public ServiceHandler(Looper looper) {
          super(looper);
      }
      
      @Override
      public void handleMessage(Message msg) {
          Update_Location();
          stopSelf(msg.arg1);

      }
  }

  @Override
  public void onCreate() {

	  HandlerThread thread = new HandlerThread("ServiceStartArguments",
    		android.os.Process.THREAD_PRIORITY_BACKGROUND);
    thread.start();
    Log.d(DEBUG_TAG, ">>>onCreate()");
    mServiceLooper = thread.getLooper();
    mServiceHandler = new ServiceHandler(mServiceLooper);
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {

      Message msg = mServiceHandler.obtainMessage();
      msg.arg1 = startId;
      mServiceHandler.sendMessage(msg);
      Log.d(DEBUG_TAG, ">>>onStartCommand()");
      return START_STICKY;
  }

  @Override
  public IBinder onBind(Intent intent) {
      return null;
  }
  
  @Override
  public void onDestroy() {
	  Log.d(DEBUG_TAG, ">>>onDestroy()");
  }

	public String Update_Location(){
		final String msg="Rating Done";
		Intent i = new Intent(RatingService.this, RatingPage.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
		return msg;

		}

	
  private class MakeToast implements Runnable {
		String txt;
		
		public MakeToast(String text){
		    txt = text;
		}
		public void run(){
		     Toast.makeText(getApplicationContext(), txt, Toast.LENGTH_SHORT).show();
		}
  }


}