package com.example.laithbzour.doctor;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.laithbzour.doctor.gcm.GcmManager;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableOperationCallback;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import tourguide.tourguide.Overlay;
import tourguide.tourguide.Pointer;
import tourguide.tourguide.ToolTip;
import tourguide.tourguide.TourGuide;


public class Show_dr extends AppCompatActivity implements OnMapReadyCallback {
    public static MobileServiceClient mClient;
    public static MobileServiceTable<doctor> Doctor_Finder;
    private MobileServiceList<doctor> result;
    public MobileServiceTable<doctor_rating> dr_rating;
    private ImageView imgProfile, back, send_location, rate_value, favorate, add_favo,Heart,Star;
    public static String dr_id;
    ProgressDialog progressDialog;
    TextView name, price, ratingvalue;
    private LocationManager mLocationManager = null;
    private String provider = null;
    private Marker mCurrentPosition = null;
    private GoogleMap mMap;
    String SENDER_ID ;
    private GoogleCloudMessaging gcm = null;
    private TourGuide mTourGuideHandler;
    int clickcount=0;
    int clickcount_1=0;
    int clickcount_2=0;
    int clickcount_3=0;
    public static AlarmManager alarms;
    public static PendingIntent R_RAting;
    private long UPDATE_INTERVAL = 10000;
    private int START_DELAY = 5;
    public static String ids,user_id,name_dr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_dr);
        getWindow().getAttributes().windowAnimations = R.anim.push_left;
        try {

            mClient = new MobileServiceClient(
                    "https://docotrfinder.azure-mobile.net/",
                    "OCecmhorpCrBqDmLWZbaTporlbzoKP57",
                    this
            );

        } catch (Exception e) {
            createAndShowDialog(e, "Error");
        }
        back = (ImageView) findViewById(R.id.backbtn);
        imgProfile = (ImageView) findViewById(R.id.profile_image);
        name = (TextView) findViewById(R.id.name_of_doctor);
        price = (TextView) findViewById(R.id.price_value);
        ratingvalue = (TextView) findViewById(R.id.rating_value);

        send_location = (ImageView) findViewById(R.id.send);
        rate_value = (ImageView) findViewById(R.id.rate);
        favorate = (ImageView) findViewById(R.id.favorate);
        add_favo = (ImageView) findViewById(R.id.add_favo);
        Heart=(ImageView)findViewById(R.id.favorate);
        Star=(ImageView)findViewById(R.id.rate);

        SENDER_ID=GcmManager.getRegId(getBaseContext());

        Intent intent = getIntent();
        final String Data = intent.getExtras().getString("id");
        final String Data_2 = Profile.user_id.toString();
        final String Data_3 = SENDER_ID.toString();
        final String Data_4 = Profile.user_name.toString();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("ID_dr", Data).commit();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("ID_User", Data_2).commit();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("User_Device_id", Data_3).commit();
        PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit().putString("user_name", Data_4).commit();

        user_id=Profile.user_id.toString();
        //
        run();
        run_2();
        // show map
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);
        //
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(Show_dr.this)
                        .setTitle("Doctor Rating")
                        .setMessage("Are you get the appointment from dr."+name_1.toString())
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                onBackPressed();

                            }
                        })
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        LayoutInflater factory = LayoutInflater.from(Show_dr.this);
                                        final View deleteDialogView = factory.inflate(
                                                R.layout.custom_rating_dialog, null);
                                        final AlertDialog deleteDialog = new AlertDialog.Builder(Show_dr.this).create();
                                        deleteDialog.setView(deleteDialogView);
                                        deleteDialogView.findViewById(R.id.positive_button).setOnClickListener(new View.OnClickListener() {

                                            @Override
                                            public void onClick(View v) {
                                                deleteDialog.dismiss();
                                                alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                                                setRecurringAlarm(getBaseContext());
                                                onBackPressed();
                                            }
                                        });

                                        deleteDialog.show();
                                    }
                                }, 3000);



                            }
                        })
                        .setIcon(R.drawable.star)
                        .show();
            }
        });


        add_favo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickcount_3=clickcount_3+1;
                if(clickcount_3==1) {
                    mTourGuideHandler.cleanUp();
                }
                else {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(Show_dr.this);
                    builder1.setMessage("Add doctor to your favorite list ?");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Add_favoraties();

                                }
                            });

                    builder1.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }

            }
        });


        Heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickcount_2=clickcount_2+1;
                if(clickcount_2==1) {
                    mTourGuideHandler.cleanUp();
                    mTourGuideHandler.setToolTip(new ToolTip()
                            .setDescription("Doctor Rating")
                            .setBackgroundColor(Color.parseColor("#4b4b4b")))
                            .playOn(Star);
                }

            }
        });

        Star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickcount_1=clickcount_1+1;
                if(clickcount_1==1) {
                    mTourGuideHandler.cleanUp();
                    mTourGuideHandler.setToolTip(new ToolTip()
                            .setTitle("Add to Favorites")
                            .setDescription("Add the doctor to your favorites list ")
                            .setBackgroundColor(Color.parseColor("#4b4b4b")))
                            .playOn(add_favo);
                }

            }
        });

        send_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickcount=clickcount+1;
                if(clickcount==1) {
                    mTourGuideHandler.cleanUp();
                    mTourGuideHandler.setToolTip(new ToolTip()
                            .setDescription("Doctor Price")
                            .setBackgroundColor(Color.parseColor("#4b4b4b")))
                            .playOn(Heart);
                }
                else
                {
                Intent intent = new Intent(Show_dr.this,Custom_notification.class);
                    startActivity(intent);

                }

            }
        });


        mTourGuideHandler
                = TourGuide.init(this)
                .with(TourGuide.Technique.Click)
                .setPointer(new Pointer())
                .setToolTip(new ToolTip().setTitle("Send Notification")
                        .setDescription("click to send notification to get a appointment from doctor")
                        .setBackgroundColor(Color.parseColor("#4b4b4b"))
                        .setShadow(true)
                        .setGravity(Gravity.BOTTOM))
                .setOverlay(new Overlay())
                .playOn(send_location);



    }

    private void setRecurringAlarm(Context context) {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, START_DELAY);
        Intent intent = new Intent(context, RatingAlarm.class);
        R_RAting = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        alarms.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() +
                        UPDATE_INTERVAL, R_RAting);
    }


    String name_1, image_profile, dr_Number,num_favorates,num_rating,dv_id;
    Double lat,lng;
    public void  run(){
        Doctor_Finder = mClient.getTable(doctor.class);
        new AsyncTask<Void, Void, Void>() {
            Intent intent = getIntent();
            final String Data = intent.getExtras().getString("id");
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final MobileServiceList<doctor> result =
                            Doctor_Finder.where().field("id").eq(Data).execute().get();
                    for (final doctor item : result) {
                        dr_id=Data.toString();
                        lat=Double.valueOf(item.latidude.toString());
                        lng=Double.valueOf(item.langitude.toString());
                        ids=item.deviceid.toString();
                        name_1=item.name.toString();
                        name_dr=item.name.toString();
                        image_profile=item.image.toString();
                        dr_Number = String.valueOf(item.mobile_number.toString());
                        num_favorates=item.price;
                        num_rating=item.rating.toString();
                    }

                } catch (Exception exception) {
                    exception.printStackTrace();
                }

                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        name.setText("Dr."+name_1);
                        price.setText(num_favorates);
                        ratingvalue.setText(num_rating);
                        Log.e("TOKEN", "dv_id : " + ids);
                         // dv_id="cWJNYzWoCzo:APA91bFAMtv6q3yBrEFeOlo2264U8aHkRqdEO-xDYDcWSHw3Duw7yBjo0acVFLvCWwqLwSUSvdzLcPfIe7htKUjhb3PsFV6alMHfOwSuhp9wwpCcuiSy8I6-cs2qV6mqg4csVleP0F8I";
                        MarkerOptions mMarkerOptions = new MarkerOptions();
                        mMarkerOptions.position(new LatLng(lat, lng));
                        mMarkerOptions.icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.placeholder));
                        mMarkerOptions.anchor(0.5f, 0.5f);
                        CircleOptions mOptions = new CircleOptions()
                                .center(new LatLng(lat, lng)).radius(1000)
                                .strokeColor(0x110000FF).strokeWidth(1).fillColor(0x110000FF);
                        mMap.addCircle(mOptions);
                        if (mCurrentPosition != null)
                            mCurrentPosition.remove();
                        mCurrentPosition = mMap.addMarker(mMarkerOptions);

                        if (mMap != null) {
                            CameraPosition camPosition = new CameraPosition.Builder()
                                    .target(new LatLng(lat, lng)).zoom(13f).build();
                            mMap.animateCamera(CameraUpdateFactory
                                    .newCameraPosition(camPosition));
                        }
                        mMap.addCircle(mOptions);
                        if (image_profile == null) {
                            imgProfile.setImageResource(R.drawable.doctor);

                        } else {

                            Bitmap img = base64ToBitmap(image_profile);
                            Drawable drawable = new BitmapDrawable(img);
                            imgProfile.setImageDrawable(drawable);
                            imgProfile.setRotation(270);
                        }


                    }
                });
            }
        }.execute();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (isProviderAvailable() && (provider != null)) {
        }
    }

    private boolean isProviderAvailable() {
        mLocationManager = (LocationManager) getSystemService(
                Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);

        provider = mLocationManager.getBestProvider(criteria, true);
        if (mLocationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            provider = LocationManager.NETWORK_PROVIDER;

            return true;
        }

        if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            provider = LocationManager.GPS_PROVIDER;
            return true;
        }

        if (provider != null) {
            return true;
        }
        return false;
    }

    // add to favo class
    public void Add_favoraties() {
        progressDialog = new ProgressDialog(Show_dr.this);
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progressdialog);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                progressDialog.dismiss();
            }
        });
        try {
            progressDialog.show();
        } catch (Exception ignored) {
        }
        final favorate_dr items = new favorate_dr();
        items.doctor_id = dr_id;
        items.user_id = Profile.user_id.toString();

        mClient.getTable(favorate_dr.class).insert(items, new TableOperationCallback<favorate_dr>() {
            @Override
            public void onCompleted(favorate_dr entity, Exception exception, ServiceFilterResponse response) {
                if (exception == null) {
                    Add_favoraties2();
                } else {


                }
            }
        });
    }

    public void Add_favoraties2() {

        final favorate_dr items = new favorate_dr();
        items.doctor_id = dr_id;
        Doctor_Finder = mClient.getTable(doctor.class);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final MobileServiceList<doctor> result =
                            Doctor_Finder.where().field("id").eq(dr_id).execute().get();
                    for (final doctor item : result) {
                        int incress_follower = Integer.valueOf(item.favo);
                        int num_of_folloers = incress_follower + 1;
                        item.favo = String.valueOf(num_of_folloers);
                        Doctor_Finder.update(item).get();
                    }

                } catch (Exception exception) {
                    exception.printStackTrace();
                    progressDialog.dismiss();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        Toast.makeText(getBaseContext(), "تم الاضافة الى المفضلة", Toast.LENGTH_LONG).show();
                    }
                });
            }
        }.execute();
    }


    double Over_All_Rating;
    int v;
    public void  run_2(){
        final ArrayList<String> all = new ArrayList<String>();

        dr_rating = mClient.getTable(doctor_rating.class);
        new AsyncTask<Void, Void, Void>() {
            Intent intent = getIntent();
            final String Data = intent.getExtras().getString("id");
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final MobileServiceList<doctor_rating> result =
                            dr_rating.where().field("dr_id").eq(Data).execute().get();
                    for (final doctor_rating item : result) {
                        v=result.getTotalCount();
                        all.add(item.rating_value);

                    }

                } catch (Exception exception) {
                    exception.printStackTrace();
                }

                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int sums=0;
                        int count=0;
                        for (Iterator<String> iterator = all.iterator(); iterator.hasNext();) {
                            String integer = (String) iterator.next();
                            int[] intArray = {Integer.parseInt(integer)};
                            for (int i=0; i<intArray.length; i++)
                            {
                                sums+=(Integer.parseInt(integer));
                                count++;
                            }
                        }
                        float all= Float.parseFloat(String.valueOf(sums))/Float.parseFloat(String.valueOf(count));
                        String s = String.format("%.1f", all);
                        if (all==0)
                        {
                            ratingvalue.setText(String.valueOf(0));

                        }
                        else
                        {
                            ratingvalue.setText(String.valueOf(s));


                        }



                    }
                });
            }
        }.execute();

    }

    // calss to convert image (from string to bitmap image)
    private Bitmap base64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }


    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if (exception.getCause() != null) {
            ex = exception.getCause();
        }
        createAndShowDialog(ex.getMessage(), title);
    }

    private void createAndShowDialog(final String message, final String title) {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);

        builder.setMessage(message);
        builder.setTitle(title);
        builder.create().show();
    }

}
